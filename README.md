# Binar FSW-Challenge-5

## Disclaimer

Pada aplikasi ini, saya memisahkan server
berdasarkan fungsionalitasnya. File index.js
merupakan server yang akan menjalankan routing halaman
dengan menggunakan port 3000 sedangkan file apiServer.js merupakan server yang
akan menjalankan routing dari API dan mengembalikan
data dalam bentuk JSON dengan menggunakan port 5000. Sehingga untuk
menjalankan web apps ini diharuskan untuk menyalakan
kedua server tersebut.

## Pages Route

```http
  GET http://localhost:3000 // Home
  GET http://localhost:3000/filter // Filter Car
  GET http://localhost:3000/search // Search Car
  GET http://localhost:3000/create // Add new Car
  GET http://localhost:3000/edit/:id // Edit Car
```

## ERD

![ERD](/public/images/Car%20Management.png)

## API Reference

### 1) Get all cars

```http
  GET http://localhost:5000/api/cars
```

#### Response

```javascript
[
  {
    id: 1,
    name: "Avanza",
    type: "Home Car",
    price: "450000",
    image: "avanza.png",
    size: "Medium",
    createdAt: "2022-04-18T01:12:06.404Z",
    updatedAt: "2022-04-18T01:12:06.404Z",
  },
  {
    id: 2,
    name: "Xenia",
    type: "Home Car",
    price: "450000",
    image: "xenia.png",
    size: "Medium",
    createdAt: "2022-04-18T01:12:06.404Z",
    updatedAt: "2022-04-18T01:12:06.404Z",
  },
  {
    id: 3,
    name: "Innova",
    type: "Home Car",
    price: "600000",
    image: "innova.png",
    size: "Medium",
    createdAt: "2022-04-18T01:12:06.404Z",
    updatedAt: "2022-04-18T01:12:06.404Z",
  },
];
```

### 2) Get car by ID

```http
  GET http://localhost:5000/api/cars/:id
```

| Parameter | Type     | Description                      |
| :-------- | :------- | :------------------------------- |
| `id`      | `Number` | **Required**. Id of car to fetch |

#### Response

```javascript
{
  "id": 1,
  "name": "Avanza",
  "type": "Home Car",
  "price": "450000",
  "image": "avanza.png",
  "size": "Medium",
  "createdAt": "2022-04-18T01:12:06.404Z",
  "updatedAt": "2022-04-18T01:12:06.404Z"
}
```

### 3) Insert new car

```http
  POST http://localhost:5000/api/cars
```

#### Request Body

| Body    | Type     | Description   |
| :------ | :------- | :------------ |
| `name`  | `String` | **Required**. |
| `type`  | `String` | **Required**. |
| `price` | `String` | **Required**. |
| `image` | `String` | **Required**. |
| `size`  | `String` | **Required**. |

#### Request Body Example

```javascript
{
  "name": "Ferrari",
  "type": "Home Car",
  "price": "450000",
  "image": "ferrari.png",
  "size": "Medium"
}
```

#### Response

```javascript
{
	"id": 5,
	"name": "Ferrari",
	"type": "Home Car",
	"price": "450000",
	"image": "ferrari.png",
	"size": "Medium",
	"createdAt": "2022-04-18T21:58:06.550Z",
	"updatedAt": "2022-04-18T21:58:06.552Z"
}
```

### 4) Update car by ID

```http
  PUT http://localhost:5000/api/cars/:id
```

#### Request Params

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `Number` | **Required**. Id of car to update |

#### Request Body

| Body    | Type     | Description   |
| :------ | :------- | :------------ |
| `name`  | `String` | **Required**. |
| `type`  | `String` | **Required**. |
| `price` | `String` | **Required**. |
| `image` | `String` | **Required**. |
| `size`  | `String` | **Required**. |

#### Request Body Example

```javascript
{
  "name": "Ferrari",
  "type": "Sports Car",
  "price": "450000",
  "image": "ferrari.png",
  "size": "Small"
}
```

#### Response

```javascript
{
	"id": 5,
	"name": "Ferrari",
	"type": "Sports Car",
	"price": "450000",
	"image": "ferrari.png",
	"size": "Small",
	"createdAt": "2022-04-18T21:58:06.550Z",
	"updatedAt": "2022-04-18T22:01:46.117Z"
}
```

### 5) Delete car by ID

```http
  DELETE http://localhost:5000/api/cars/:id
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `Number` | **Required**. Id of car to delete |

#### Response

```javascript
No body returned for response
```
