"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert("Cars", [
      {
        name: "Avanza",
        type: "Home Car",
        price: "450000",
        image: "avanza.png",
        size: "Medium",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Xenia",
        type: "Home Car",
        price: "450000",
        image: "xenia.png",
        size: "Medium",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Innova",
        type: "Home Car",
        price: "600000",
        image: "innova.png",
        size: "Medium",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("Cars", null, {});
  },
};
