const express = require("express");
const expressLayouts = require("express-ejs-layouts");
const path = require("path");
const route = require("./routes/index");
const methodOverride = require("method-override");
const app = express();
const port = 3000;
const session = require("express-session");
const flash = require("connect-flash");

// Public Path
const publicDirectory = path.join(__dirname, "public");

// Body Parser
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Use Method Override
app.use(methodOverride("_method"));

// View Engine, Layouts, Static Serve
app.set("view engine", "ejs");
app.use(expressLayouts);
app.use(express.static(publicDirectory));

// Set Session and Use Flash
app.use(
  session({
    secret: "SecretForSession",
    cookie: { maxAge: 60000 },
    resave: false,
    saveUninitialized: false,
  })
);
app.use(flash());

// Routing
app.use(route);

app.listen(port, () => {
  console.log(`Server running on localhost:${port}`);
});
