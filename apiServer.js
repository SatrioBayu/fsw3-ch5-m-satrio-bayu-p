const express = require("express");
const carsRoute = require("./routes/cars");
const app = express();
const port = 5000;

app.use(express.json());

app.use("/api/cars", carsRoute);

app.listen(port, () => {
  console.log(`Server running on localhost:${port}`);
});
