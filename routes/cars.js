const express = require("express");
const router = express.Router();
const carsController = require("../controllers").cars;

router.get("/", carsController.list);
router.get("/:id", carsController.getDataById);
router.post("/", carsController.add);
router.put("/:id", carsController.update);
router.delete("/:id", carsController.deleteData);

module.exports = router;
