function openNav() {
  document.getElementById("sidebar-extension").style.width = "150px";
  document.getElementById("sidebar-extension").style.display = "flex";
}
function closeNav() {
  document.getElementById("sidebar-extension").style.width = "0";
  document.getElementById("sidebar-extension").style.padding = "0";
  document.getElementById("main").style.marginLeft = "50px";
}
function openNavMarg() {
  document.getElementById("sidebar-extension").style.width = "150px";
  document.getElementById("sidebar-extension").style.display = "flex";
  document.getElementById("main").style.marginLeft = "200px";
}
function enabled() {
  document.querySelector("#btn-edit-save").disabled = false;
}
